//
//  SuitTests.swift
//  BlackJackTests
//
//  Created by Kei Ishida on 2019/10/13.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

@testable import BlackJack
import XCTest

class SuitTests: XCTestCase {
    override func setUp() {}

    override func tearDown() {}

    func test要素を全部列挙すると4種類() {
        XCTAssertEqual(4, Suit.allSuits.count)
    }

    func test要素を全部列挙して重複なし() {
        let suits = Set<Suit>(Suit.allSuits)
        XCTAssertEqual(Suit.allSuits.count, suits.count)
    }
}
