//
//  HandTests.swift
//  BlackJackTests
//
//  Created by Kei Ishida on 2019/10/13.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

@testable import BlackJack
import XCTest

class HandTests: XCTestCase {
    override func setUp() {
        self.hand = Hand()
    }

    override func tearDown() {
        self.hand = nil
    }

    var hand: Hand!

    func testInit後は何もない() {
        XCTAssertTrue(self.hand.isEmpty)
        XCTAssertEqual(0, self.hand.count)
        XCTAssertEqual(0, self.hand.value)
    }

    func testInit後はバーストしてない() {
        XCTAssertFalse(self.hand.isBust)
    }

    func testAddしたら増える() {
        self.hand.add(card: Card(suit: Suit.heart, rank: Rank(rawValue: 1)!))
        XCTAssertEqual(1, self.hand.count)
        XCTAssertEqual(Card(suit: Suit.heart, rank: Rank(rawValue: 1)!), self.hand.card(at: 0))
        XCTAssertEqual(1, self.hand.value)
    }

    func test複数Addして点数の合計が合う() {
        self.hand.add(card: Card(suit: Suit.diamond, rank: Rank(rawValue: 1)!))
        XCTAssertEqual(1, self.hand.value)
        self.hand.add(card: Card(suit: Suit.spade, rank: Rank(rawValue: 10)!))
        XCTAssertEqual(11, self.hand.value)
        self.hand.add(card: Card(suit: Suit.club, rank: Rank(rawValue: 13)!))
        XCTAssertEqual(21, self.hand.value)
    }

    func test複数を適当にAddして個別のカードの点数の合計と点数が合う() {
        let deck = Deck()
        var sum = 0
        while deck.canDraw {
            let card = deck.draw()
            sum += card.value
            self.hand.add(card: card)
            XCTAssertEqual(sum, self.hand.value)
        }
    }

    func test複数Addして21点未満ならバーストしてない() {
        self.hand.add(card: Card(suit: Suit.diamond, rank: Rank(rawValue: 1)!))
        XCTAssertFalse(self.hand.isBust)
        self.hand.add(card: Card(suit: Suit.spade, rank: Rank(rawValue: 9)!))
        XCTAssertFalse(self.hand.isBust)
        self.hand.add(card: Card(suit: Suit.heart, rank: Rank(rawValue: 13)!))
        XCTAssertFalse(self.hand.isBust)
    }

    func test複数Addして21点ならバーストしてない() {
        self.hand.add(card: Card(suit: Suit.diamond, rank: Rank(rawValue: 1)!))
        self.hand.add(card: Card(suit: Suit.spade, rank: Rank(rawValue: 10)!))
        self.hand.add(card: Card(suit: Suit.heart, rank: Rank(rawValue: 12)!))
        XCTAssertFalse(self.hand.isBust)
    }

    func test複数Addして21点超えるとバースト() {
        self.hand.add(card: Card(suit: Suit.heart, rank: Rank(rawValue: 3)!))
        self.hand.add(card: Card(suit: Suit.spade, rank: Rank(rawValue: 9)!))
        self.hand.add(card: Card(suit: Suit.club, rank: Rank(rawValue: 11)!))
        XCTAssertTrue(self.hand.isBust)
    }

    func test複数を適当にAddして21点以内ならバーストしない() {
        let deck = Deck()
        var sum = 0
        while deck.canDraw {
            let card = deck.draw()
            sum += card.value
            self.hand.add(card: card)
            if sum > 21 {
                return
            }
            XCTAssertFalse(self.hand.isBust)
        }
    }

    func test複数を適当にAddして21点超えるとバースト() {
        let deck = Deck()
        var sum = 0
        while deck.canDraw {
            let card = deck.draw()
            sum += card.value
            self.hand.add(card: card)
            if sum <= 21 {
                continue
            }
            XCTAssertTrue(self.hand.isBust)
        }
    }
}
