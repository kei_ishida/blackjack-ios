//
//  DeckTests.swift
//  BlackJackTests
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

@testable import BlackJack
import XCTest

class DeckTests: XCTestCase {
    override func setUp() {
        self.deck = Deck()
    }

    override func tearDown() {
        self.deck = nil
    }

    var deck: Deck!

    func testInit後52枚ある() {
        XCTAssertEqual(52, self.deck.count)
    }

    func test引くと枚数が減る() {
        _ = self.deck.draw()
        XCTAssertEqual(51, self.deck.count)
    }

    func test52回引ける() {
        for i in 0 ..< 52 {
            XCTAssertTrue(self.deck.canDraw, "\(i)")
            _ = self.deck.draw()
        }
    }

    func test53回引けない() {
        for _ in 0 ..< 52 {
            _ = self.deck.draw()
        }
        XCTAssertFalse(self.deck.canDraw)
    }

    func test52回引いたのが重複しない() {
        var cards = Set<Card>()
        for _ in 0 ..< 52 {
            cards.insert(self.deck.draw())
        }
        XCTAssertEqual(52, cards.count)
    }
}
