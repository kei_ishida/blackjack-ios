//
//  RankTests.swift
//  BlackJackTests
//
//  Created by Kei Ishida on 2019/10/13.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

@testable import BlackJack
import XCTest

class RankTests: XCTestCase {
    override func setUp() {}

    override func tearDown() {}

    func testありえる値を全部列挙すると13種類() {
        XCTAssertEqual(13, Rank.allRanks.count)
    }

    func testありえる値を全部列挙して重複なし() {
        let ranks = Set<Rank>(Rank.allRanks)
        XCTAssertEqual(Rank.allRanks.count, ranks.count)
    }

    func test等しいはずのが等しい() {
        for i in 1 ..< 14 {
            let rank1 = Rank(rawValue: i)
            let rank2 = Rank(rawValue: i)
            XCTAssertEqual(rank1, rank2)
        }
    }

    func test等しくないはずのが等しくない() {
        for i in 1 ..< 14 {
            let rank1 = Rank(rawValue: i)
            for j in 1 ..< 14 {
                if i == j {
                    continue
                }
                let rank2 = Rank(rawValue: j)
                XCTAssertNotEqual(rank1, rank2)
            }
        }
    }

    func test数札の点数が正しい() {
        for i in 1 ..< 11 {
            XCTAssertEqual(i, Rank(rawValue: i)!.value)
        }
    }

    func test絵札の点数が正しい() {
        for i in 11 ..< 14 {
            XCTAssertEqual(10, Rank(rawValue: i)!.value)
        }
    }
}
