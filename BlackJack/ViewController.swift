//
//  ViewController.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var startGameButton: UIButton!
    @IBOutlet var drawButton: UIButton!
    @IBOutlet var stayButton: UIButton!
    @IBOutlet var historyTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateGameStatus()
    }

    @IBAction func startGameButtonDidTap(_: Any) {
        self.game = Game()
        self.updateGameStatus()
    }

    @IBAction func drawButtonDidTap(_: Any) {
        self.game?.draw()
        self.updateGameStatus()
    }

    @IBAction func stayButtonDidTap(_: Any) {
        self.game?.stay()
        self.updateGameStatus()
    }

    private func updateGameStatus() {
        guard let game = self.game else {
            self.startGameButton.isEnabled = true
            self.drawButton.isEnabled = false
            self.stayButton.isEnabled = false
            return
        }

        self.startGameButton.isEnabled = game.completed
        self.drawButton.isEnabled = game.canDraw
        self.stayButton.isEnabled = game.canStay

        self.historyTextView.text = game.history
    }

    var game: Game?
}
