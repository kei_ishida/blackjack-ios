//
//  Rank.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

struct Rank {
    init?(rawValue: Int) {
        if rawValue <= 0 {
            return nil
        }
        if rawValue > 13 {
            return nil
        }

        self.rawValue = rawValue
    }

    var title: String {
        switch self.rawValue {
        case 1:
            return "A"
        case 11:
            return "J"
        case 12:
            return "Q"
        case 13:
            return "K"
        default:
            return String(self.rawValue)
        }
    }

    var value: Int {
        if self.rawValue < 11 {
            return self.rawValue
        }

        return 10
    }

    static var allRanks: [Rank] = {
        Rank.pipRanks + Rank.faceRanks
    }()

    static var pipRanks: [Rank] = {
        (1 ..< 11).compactMap { Rank(rawValue: $0) }
    }()

    static var faceRanks: [Rank] = {
        (11 ..< 14).compactMap { Rank(rawValue: $0) }
    }()

    private var rawValue: Int
}

extension Rank: Equatable {
    static func == (lhs: Rank, rhs: Rank) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}

extension Rank: Hashable {}
