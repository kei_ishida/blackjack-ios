//
//  Deck.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

struct Card {
    init(suit: Suit, rank: Rank) {
        self.suit = suit
        self.rank = rank
    }

    var title: String {
        return self.suit.title + self.rank.title
    }

    var value: Int { return self.rank.value }

    let suit: Suit
    let rank: Rank
}

extension Card: Hashable {
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.suit == rhs.suit && lhs.rank == rhs.rank
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(self.suit)
        hasher.combine(self.rank)
    }
}
