//
//  Game.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

class Game {
    init() {
        self.user.draw(from: self.deck)
        self.addHistoryAboutLastDrawnCard(of: self.user, showsCardTitle: true)

        self.dealer.draw(from: self.deck)
        self.addHistoryAboutLastDrawnCard(of: self.dealer, showsCardTitle: true)

        self.user.draw(from: self.deck)
        self.addHistoryAboutLastDrawnCard(of: self.user, showsCardTitle: true)

        self.dealer.draw(from: self.deck)
        self.addHistoryAboutLastDrawnCard(of: self.dealer, showsCardTitle: false)
    }

    var canDraw: Bool {
        return !self.completed && self.deck.canDraw }

    var canStay: Bool { return !self.completed }
    var history: String {
        return self.historyLines.joined(separator: "\n")
    }
    var completed: Bool = false

    func draw() {
        guard self.canDraw else {
            return
        }
        self.user.draw(from: self.deck)
        self.addHistoryAboutLastDrawnCard(of: self.user, showsCardTitle: true)
        if self.user.isBust {
            self.judge()
        }
    }

    func stay() {
        guard self.canStay else {
            return
        }
        self.addHistoryAboutLastDrawnCard(of: self.dealer, showsCardTitle: true)
        while self.dealer.needsDraw && self.deck.canDraw {
            self.dealer.draw(from: self.deck)
            self.addHistoryAboutLastDrawnCard(of: self.dealer, showsCardTitle: true)
        }
        self.judge()
    }

    private var historyLines: [String] = []

    private let deck = Deck()
    private let dealer = Dealer()
    private let user = User()

    private func addHistoryAboutLastDrawnCard(of player: Player, showsCardTitle: Bool) {
        guard let _ = player.lastDrawnCard else {
            return
        }
        var historyLine = "\(player.title)の\(player.numberOfHand)枚目:"
        if showsCardTitle {
            historyLine = historyLine + " \(player.lastDrawnCardTitle!)"
            historyLine = historyLine + " 合計:\(player.value)点"
        } else {
            historyLine = historyLine + " ないしょ"
        }
        self.historyLines.append(historyLine)
    }

    private func judge() {
        defer {
            self.completed = true
        }

        if self.user.isBust {
            self.historyLines.append("バストしました。あなたの負けです")
            return
        }

        self.historyLines.append("ディーラー \(self.dealer.value)点")
        self.historyLines.append("あなた \(self.user.value)点")
        if !self.dealer.isBust && self.user.value < self.dealer.value {
            self.historyLines.append("あなたの負けです。惜しい！")
            return
        }
        if self.dealer.isBust || self.user.value > self.dealer.value {
            self.historyLines.append("あなたの勝ちです。素晴らしいです！")
            return
        }
        self.historyLines.append("引き分けです")
    }
}
