//
//  Suit.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

enum Suit: CaseIterable {
    case diamond
    case club
    case heart
    case spade

    static var allSuits: [Suit] {
        return Suit.allCases
    }

    var title: String {
        switch self {
        case .diamond:
            return "ダイヤ"
        case .club:
            return "クラブ"
        case .heart:
            return "ハート"
        case .spade:
            return "スペード"
        }
    }
}
