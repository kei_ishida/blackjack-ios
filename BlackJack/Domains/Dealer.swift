//
//  Dealer.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

class Dealer: Player {
    var needsDraw: Bool {
        return self.numberOfHand < 2 || self.value < 17
    }

    override var title: String {
        return "ディーラー"
    }
}
