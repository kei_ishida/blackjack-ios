//
//  Hand.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/13.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

struct Hand {
    mutating func add(card: Card) {
        self.cards.append(card)
        self.value += card.value
    }

    func card(at: Int) -> Card {
        return self.cards[at]
    }

    func title(at: Int) -> String {
        return self.card(at: at).title
    }

    var count: Int {
        return self.cards.count
    }

    var isEmpty: Bool {
        return self.cards.isEmpty
    }

    private(set) var value: Int = 0

    var isBust: Bool {
        return self.value > 21
    }

    private var cards: [Card] = []
}
