//
//  Player.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

class Player {
    init() {}

    var value: Int {
        return self.hand.value
    }

    func draw(from: Deck) {
        let drawn = from.draw()
        self.hand.add(card: drawn)
        self.lastDrawnCard = drawn
    }

    var numberOfHand: Int {
        return self.hand.count
    }

    func handTitle(at: Int) -> String {
        return self.hand.title(at: at)
    }

    private(set) var lastDrawnCard: Card?

    var lastDrawnCardTitle: String? {
        return self.lastDrawnCard?.title
    }

    var isBust: Bool {
        return self.hand.isBust
    }

    dynamic var title: String {
        return ""
    }

    private var hand = Hand()
}
