//
//  Deck.swift
//  BlackJack
//
//  Created by Kei Ishida on 2019/10/12.
//  Copyright © 2019 Kei Ishida. All rights reserved.
//

import Foundation

class Deck {
    init() {
        self.add(cards: Suit.allSuits.reduce([], { (previous, suit) -> [Card] in
            previous + Rank.allRanks.map({ rank -> Card in
                Card(suit: suit, rank: rank)
            })
        }))
        self.cards.shuffle()
    }

    var count: Int {
        return self.cards.count
    }

    var canDraw: Bool {
        return !self.cards.isEmpty
    }

    func draw() -> Card {
        return self.cards.removeFirst()
    }

    private func add(card: Card) {
        self.cards.append(card)
    }

    private func add(cards: [Card]) {
        self.cards.append(contentsOf: cards)
    }

    private var cards: [Card] = []
}
